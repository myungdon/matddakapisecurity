package com.mj.matddakapi.entity;

import com.mj.matddakapi.interfaces.CommonModelBuilder;
import com.mj.matddakapi.model.store.StoreChangeRequest;
import com.mj.matddakapi.model.store.StoreCreateRequest;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Store {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String storeName;

    @Column(nullable = false, length = 100)
    private String addressStoreDo;

    @Column(length = 100)
    private String addressStoreG;

    @Column(nullable = false)
    private Double xStore;

    @Column(nullable = false)
    private Double yStore;

    @Column(nullable = false, length = 15)
    private String storeNumber;

    @Column(nullable = false, length = 50)
    private String sellTime;

    @Column(nullable = false, length = 30)
    private String holiday;

    @Column(nullable = false, length = 50)
    private String deliveryArea;

    @Column(nullable = false, length = 20)
    private String bossName;

    @Column(nullable = false, length = 30)
    private String shopId;

    /**
     *
     * @param request 가게 정보 변경
     */
    public void putStore(StoreChangeRequest request) {
        this.storeName = request.getStoreName();
        this.addressStoreDo = request.getAddressStoreDo();
        this.addressStoreG = request.getAddressStoreG();
        this.xStore = request.getXStore();
        this.yStore = request.getYStore();
        this.storeNumber = request.getStoreNumber();
        this.sellTime = request.getSellTime();
        this.holiday = request.getHoliday();
        this.deliveryArea = request.getDeliveryArea();
        this.bossName = request.getBossName();
        this.shopId = request.getShopId();
    }

    /**
     *
     * @param builder 가게 정보 등록용
     */
    private Store(Builder builder) {
        this.storeName = builder.storeName;
        this.addressStoreDo = builder.addressStoreDo;
        this.addressStoreG = builder.addressStoreG;
        this.xStore = builder.xStore;
        this.yStore = builder.yStore;
        this.storeNumber = builder.storeNumber;
        this.sellTime = builder.sellTime;
        this.holiday = builder.holiday;
        this.deliveryArea = builder.deliveryArea;
        this.bossName = builder.bossName;
        this.shopId = builder.shopId;
    }


    public static class Builder implements CommonModelBuilder<Store> {

        private final String storeName;
        private final String addressStoreDo;
        private final String addressStoreG;
        private final Double xStore;
        private final Double yStore;
        private final String storeNumber;
        private final String sellTime;
        private final String holiday;
        private final String deliveryArea;
        private final String bossName;
        private final String shopId;

        public Builder(StoreCreateRequest request) {
            this.storeName = request.getStoreName();
            this.addressStoreDo = request.getAddressStoreDo();
            this.addressStoreG = request.getAddressStoreG();
            this.xStore = request.getXStore();
            this.yStore = request.getYStore();
            this.storeNumber = request.getStoreNumber();
            this.sellTime = request.getSellTime();
            this.holiday = request.getHoliday();
            this.deliveryArea = request.getDeliveryArea();
            this.bossName = request.getBossName();
            this.shopId = request.getShopId();
        }

        @Override
        public Store build() {
            return new Store(this);
        }
    }

    /**
     *
     * @param builder 대량 등록용
     */
    private Store(BuilderPer builder) {
        this.storeName = builder.storeName;
        this.addressStoreDo = builder.addressStoreDo;
        this.addressStoreG = builder.addressStoreG;
        this.xStore = builder.xStore;
        this.yStore = builder.yStore;
        this.storeNumber = builder.storeNumber;
        this.sellTime = builder.sellTime;
        this.holiday = builder.holiday;
        this.deliveryArea = builder.deliveryArea;
        this.bossName = builder.bossName;
        this.shopId = builder.shopId;

    }

    public static class BuilderPer implements CommonModelBuilder<Store> {
        private final String storeName;
        private final String addressStoreDo;
        private final String addressStoreG;
        private final Double xStore;
        private final Double yStore;
        private final String storeNumber;
        private final String sellTime;
        private final String holiday;
        private final String deliveryArea;
        private final String bossName;
        private final String shopId;
        public BuilderPer(String[] cols) {
            this.addressStoreDo = cols[0];
            this.addressStoreG = cols[1];
            this.storeName = cols[2];
            this.storeNumber = cols[3];
            this.xStore = Double.parseDouble(cols[4]);
            this.yStore = Double.parseDouble(cols[5]);
            this.sellTime = cols[6];
            this.holiday = cols[7];
            this.deliveryArea = cols[8];
            this.bossName = cols[9];
            this.shopId = cols[10];
        }
        @Override
        public Store build() {
            return new Store(this);
        }
    }
}