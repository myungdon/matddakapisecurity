package com.mj.matddakapi.exception;

public class CEmailFormException extends RuntimeException{
    public CEmailFormException(String msg, Throwable t) {
        super(msg, t);
    }

    public CEmailFormException(String msg) {
        super(msg);
    }

    public CEmailFormException() {
        super();
    }
}
