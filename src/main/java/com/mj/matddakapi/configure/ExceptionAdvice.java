package com.mj.matddakapi.configure;

import com.mj.matddakapi.enums.ResultCode;
import com.mj.matddakapi.exception.CAgeUnderException;
import com.mj.matddakapi.exception.CPwDiffException;
import com.mj.matddakapi.exception.CRiderMissingException;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.service.ResponseService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected CommonResult defaultException(HttpServletRequest request, Exception e){
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CAgeUnderException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customAUException(HttpServletRequest request, Exception e){
        return ResponseService.getFailResult(ResultCode.AGE_UNDER);
    }

    @ExceptionHandler(CRiderMissingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customRMException(HttpServletRequest request, Exception e){
        return ResponseService.getFailResult(ResultCode.RIDER_MISSING);
    }

    @ExceptionHandler(CPwDiffException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customPDException(HttpServletRequest request, Exception e){
        return ResponseService.getFailResult(ResultCode.PASSWORD_DIFFERENT);
    }
}
