package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.entity.RiderPay;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.riderpay.RiderPayItem;
import com.mj.matddakapi.model.riderpay.RiderPayResponse;
import com.mj.matddakapi.repository.RiderPayRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RiderPayService {
    private final RiderPayRepository riderPayRepository;

    /**
     * 라이더 페이 복수
     */
    public ListResult<RiderPayItem> getRiderPays() {
        List<RiderPay> originList = riderPayRepository.findAll();

        List<RiderPayItem> result = new LinkedList<>();

        for (RiderPay riderPay : originList) result.add(new RiderPayItem.Builder(riderPay).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 라이더 페이 단수
     */

    public RiderPayResponse getRiderPay(long riderPayId) {
        RiderPay originData = riderPayRepository.findById(riderPayId).orElseThrow();

        return new RiderPayResponse.Builder(originData).build();
    }

    /**
     * 라이더 페이 복수 페이징
     */
    public ListResult<RiderPayItem> getRiderPayP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<RiderPay> riderPays = riderPayRepository.findAllByOrderByIdAsc(pageRequest);

        List<RiderPayItem> response = new LinkedList<>();
        for (RiderPay riderPay : riderPays) response.add(new RiderPayItem.Builder(riderPay).build());

        return ListConvertService.settingResult(response, riderPays.getTotalElements(), riderPays.getTotalPages(), riderPays.getPageable().getPageNumber());
    }
}
