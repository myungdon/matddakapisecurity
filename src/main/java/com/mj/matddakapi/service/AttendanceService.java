package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Attendance;
import com.mj.matddakapi.enums.attendance.StateWork;
import com.mj.matddakapi.model.attendance.AttendanceResponse;
import com.mj.matddakapi.repository.AttendanceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AttendanceService {
    private final AttendanceRepository attendanceRepository;

    /**
     * 근태 단수
     */
    public AttendanceResponse getAttendance(long riderId) {
        Optional<Attendance> originData = attendanceRepository.findByRider_Id(riderId);

        return new AttendanceResponse.Builder(originData.get()).build();
    }

    /**
     * 근태 휴식 상태 변경
     */
    public void putAttendancePause(long attendanceId) {
        Attendance originData = attendanceRepository.findById(attendanceId).orElseThrow();
        originData.setStateWork(StateWork.PAUSE_WORK);
        originData.setDateCheckWork(LocalDateTime.now());

        attendanceRepository.save(originData);
    }

    /**
     * 근태 운행 상태 변경
     */
    public void putAttendanceStart(long attendanceId) {
        Attendance originData = attendanceRepository.findById(attendanceId).orElseThrow();
        originData.setStateWork(StateWork.STRAT_WORK);
        originData.setDateCheckWork(LocalDateTime.now());

        attendanceRepository.save(originData);
    }

    /**
     * 근태 퇴근 상태 변경
     */
    public void putAttendanceStop(long attendanceId) {
        Attendance originData = attendanceRepository.findById(attendanceId).orElseThrow();
        originData.setStateWork(StateWork.STOP_WORK);
        originData.setDateCheckWork(LocalDateTime.now());

        attendanceRepository.save(originData);
    }
}
