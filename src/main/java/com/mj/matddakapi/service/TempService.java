package com.mj.matddakapi.service;

import com.google.api.client.util.Value;
import com.google.cloud.storage.BlobInfo;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.entity.Store;
import com.mj.matddakapi.exception.CRiderMissingException;
import com.mj.matddakapi.lib.CommonFile;
import com.mj.matddakapi.model.rider.person.RiderImgUpdateRequest;
import com.mj.matddakapi.repository.RiderRepository;
import com.mj.matddakapi.repository.StoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TempService {
    private final StoreRepository storeRepository;
    private final RiderRepository riderRepository;

    /**
     *
     * @param multipartFile 데이터 베이스에 가게 정보 한번에 업로드
     * @throws IOException
     */


    public void setStoreByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        // 버퍼에서 넘어온 string 한줄을 임시로 담아 둘 변수
        String line = "";
        // 줄 번호 수동체크 하기 위해 int로 줄 번호 1씩 증가해서 기록 해 둘 변수
        int index = 0;

        // 빈 값이 아닐 때까지 넣겠다
        while ((line = bufferedReader.readLine()) != null){
            if (index > 0) {
                String[] cols = line.split(",");
                if (cols.length == 6){

                    // db에 넣기
                    storeRepository.save(new Store.BuilderPer(cols).build());
                }
            }
            index++;
        }

        bufferedReader.close();
    }

    // 파일 받는 메소드
    public void setRiderImgByFile(MultipartFile multipartFile) throws IOException {
        // 받은 파일
        File imgFile = CommonFile.multipartToFile(multipartFile);

        // 이미지 버퍼로 읽음
        BufferedImage bufferedImage = ImageIO.read(imgFile);

        imageResize(bufferedImage);
}

    // 리사이즈 실행 메소드
    public void imageResize(BufferedImage bufferedImage) throws IOException {
        // 이미지 세로와 가로 크기를 가져옴
        int originWidth = bufferedImage.getWidth();
        int originHeight = bufferedImage.getHeight();

        // 리사이즈 할 변수 설정
        int newWidth;
        int newHeight;

        // 파일 크기 변경
        if (originWidth <= 1000 && originHeight <= 1000) { // 가로 세로 둘다 1000보다 작거나 같으면 원본 크기로
            newWidth = originWidth;
            newHeight = originHeight;
        } else if (originWidth > originHeight) { // 넓이가 높이 보다 크면 넓이에 크기 맞추기
            newWidth = 1000;
            newHeight = 1000 * originHeight / originWidth;
        } else { // 아니면 높이에 크기 맞추기
            newWidth = 1000 * originWidth / originHeight;
            newHeight = 1000;
        }

        makeNewImage(newWidth, newHeight, bufferedImage);
    }


    // 새로운 이미지 생성
    public void makeNewImage(int newWidth, int newHeight, BufferedImage bufferedImage) throws IOException {
        Image resizeImage = bufferedImage.getScaledInstance(newWidth, newHeight, Image.SCALE_FAST);
        BufferedImage newImg = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_CUSTOM);
        Graphics2D graphics = newImg.createGraphics();
        graphics.drawImage(resizeImage, 0, 0, null); // 그리기
        graphics.dispose(); // 자원해제

        // 이미지 저장
        File newFile = new File("src/image.jpg");
        ImageIO.write(newImg, "jpg", newFile);
    }
}
