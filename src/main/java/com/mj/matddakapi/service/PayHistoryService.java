package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.PayHistory;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.entity.RiderPay;
import com.mj.matddakapi.enums.payHistory.PayType;
import com.mj.matddakapi.model.payhistory.PayHistoryCreateRequest;
import com.mj.matddakapi.repository.PayHistoryRepository;
import com.mj.matddakapi.repository.RiderPayRepository;
import com.mj.matddakapi.repository.RiderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PayHistoryService {
    private final PayHistoryRepository payHistoryRepository;
    private final RiderPayRepository riderPayRepository;
    private final RiderRepository riderRepository;

    /**
     * 페이 증가
     */
    public void setPayIn(Rider rider, PayHistoryCreateRequest request) {
        PayHistory addData = new PayHistory();
        addData.setRider(rider);
        addData.setPayType(PayType.IN);
        addData.setPayAmount(request.getPayAmount());
        addData.setDatePayRenewal(LocalDateTime.now());

        payHistoryRepository.save(addData);

        RiderPay originData = riderPayRepository.findById(addData.getRider().getId()).orElseThrow();
        originData.setPayNow(originData.getPayNow()+ addData.getPayAmount());

        riderPayRepository.save(originData);
    }

    /**
     * 페이 감소
     * 감소 금액이 현재 페이 보다 작으면 예외 처리
     */
    public void setPayOut(Rider rider, PayHistoryCreateRequest request) throws Exception {
        Optional<Rider> rider1 = riderRepository.findById(rider.getId());
        Optional<RiderPay> riderPay = riderPayRepository.findById(rider1.get().getId());

        if (riderPay.get().getPayNow() < request.getPayAmount()) throw new Exception();
        PayHistory addData = new PayHistory();
        addData.setRider(rider);
        addData.setPayType(PayType.OUT);
        addData.setPayAmount(request.getPayAmount());
        addData.setDatePayRenewal(LocalDateTime.now());

        payHistoryRepository.save(addData);

        RiderPay originData = riderPayRepository.findById(addData.getRider().getId()).orElseThrow();
        originData.setPayNow(originData.getPayNow() - addData.getPayAmount());

        riderPayRepository.save(originData);

    }
}