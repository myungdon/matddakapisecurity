package com.mj.matddakapi.enums.delivery;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StateDelivery {
    R_PICK("배차")
    ,GO("출발")
    ,DONE("완료")
    ,R_CANCEL("취소");

    private final String name;
}
