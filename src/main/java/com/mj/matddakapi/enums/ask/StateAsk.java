package com.mj.matddakapi.enums.ask;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StateAsk {
    REQUESTING("요청")
    ,S_PICK("요청완료")
    ,S_CANCEL("주문취소");

    private final String name;
}
