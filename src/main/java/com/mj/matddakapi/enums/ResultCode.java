package com.mj.matddakapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0,"성공하였습니다.")
    ,FAILED(-1,"실패하였습니다.")

    ,AGE_UNDER(-1006, "15세 미만은 가입 할 수 없습니다.")

    ,MONEY_OUT_OVER(-6000, "출금 가능한 금액을 초과하였습니다.")

    ,RIDER_MISSING(-7000, "회원정보를 찾을 수 없습니다.")
    ,PASSWORD_DIFFERENT(-7001, "비밀번호를 확인 해 주십시요.")

    ,MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")
    ,WRONG_PHONE_NUMBER(-10001, "잘못된 핸드폰 번호입니다.");

    private final Integer code;
    private final String msg;
}
