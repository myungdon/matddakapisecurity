package com.mj.matddakapi.model.riderpay;

import com.mj.matddakapi.entity.RiderPay;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import lombok.*;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RiderPayItem {
    private Long id;
    private Long riderId;
    private String riderName;
    private Double payNow;

    public RiderPayItem(Builder builder) {
        this.id = builder.id;
        this.riderId = builder.riderId;
        this.riderName = builder.riderName;
        this.payNow = builder.payNow;
    }

    public static class Builder implements CommonModelBuilder<RiderPayItem> {
        private final Long id;
        private final Long riderId;
        private final String riderName;
        private final Double payNow;

        public Builder(RiderPay riderPay) {
            this.id = riderPay.getId();
            this.riderId = riderPay.getRider().getId();
            this.riderName = riderPay.getRider().getName();
            this.payNow = riderPay.getPayNow();
        }
        @Override
        public RiderPayItem build() {
            return new RiderPayItem(this);
        }
    }
}
