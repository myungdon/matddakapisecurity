package com.mj.matddakapi.model.board;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardCreateRequest {
    private String title;
    private String img;
    private String text;
}
