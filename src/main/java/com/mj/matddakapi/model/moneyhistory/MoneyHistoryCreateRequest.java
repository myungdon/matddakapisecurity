package com.mj.matddakapi.model.moneyhistory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoneyHistoryCreateRequest {
    private Double moneyAmount;
    private String etcMemo;
}
