package com.mj.matddakapi.model.store;

import com.mj.matddakapi.entity.Store;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreResponse {
    private Long id;
    private String storeName;
    private String addressStoreDo;
    private String addressStoreG;
    private Double xStore;
    private Double yStore;
    private String storeNumber;
    private String sellTime;
    private String holiday;
    private String deliveryArea;
    private String bossName;
    private String shopId;

    private StoreResponse(Builder builder) {
        this.id = builder.id;
        this.storeName = builder.storeName;
        this.addressStoreDo = builder.addressStoreDo;
        this.addressStoreG = builder.addressStoreG;
        this.xStore = builder.xStore;
        this.yStore = builder.yStore;
        this.storeNumber = builder.storeNumber;
        this.sellTime = builder.sellTime;
        this.holiday = builder.holiday;
        this.deliveryArea = builder.deliveryArea;
        this.bossName = builder.bossName;
        this.shopId = builder.shopId;
    }

    public static class Builder implements CommonModelBuilder<StoreResponse> {
        private final Long id;
        private final String storeName;
        private final String addressStoreDo;
        private final String addressStoreG;
        private final Double xStore;
        private final Double yStore;
        private final String storeNumber;
        private final String sellTime;
        private final String holiday;
        private final String deliveryArea;
        private final String bossName;
        private final String shopId;

        public Builder(Store store) {
            this.id = store.getId();
            this.storeName = store.getStoreName();
            this.addressStoreDo = store.getAddressStoreDo();
            this.addressStoreG = store.getAddressStoreG();
            this.xStore = store.getXStore();
            this.yStore = store.getYStore();
            this.storeNumber = store.getStoreNumber();
            this.sellTime = store.getSellTime();
            this.holiday = store.getHoliday();
            this.deliveryArea = store.getDeliveryArea();
            this.bossName = store.getBossName();
            this.shopId = store.getShopId();
        }

        @Override
        public StoreResponse build() {
            return new StoreResponse(this);
        }
    }
}
