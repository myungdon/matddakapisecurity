package com.mj.matddakapi.model.delivery;

import com.mj.matddakapi.entity.Delivery;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DeliveryResponse {
    private Long id;
    private Long riderId;
    private String riderName;
    private Long askId;
    private String storeName;
    private String addressStoreDo;
    private String addressClientDo;
    private String stateDelivery;
    private Double distance;
    private Double priceFood;
    private Double feeTotal;
    private String isPay;
    private String requestRider;
    private String datePick;
    private String dateGo;
    private String dateDone;
    private String dateCancel;

    public DeliveryResponse(Builder builder) {
        this.id = builder.id;
        this.riderId = builder.riderId;
        this.riderName = builder.riderName;
        this.askId = builder.askId;
        this.storeName = builder.storeName;
        this.addressStoreDo = builder.addressStoreDo;
        this.addressClientDo = builder.addressClientDo;
        this.stateDelivery = builder.stateDelivery;
        this.distance = builder.distance;
        this.priceFood = builder.priceFood;
        this.feeTotal = builder.feeTotal;
        this.isPay = builder.isPay;
        this.requestRider = builder.requestRider;
        this.datePick = builder.datePick;
        this.dateGo = builder.dateGo;
        this.dateDone = builder.dateDone;
        this.dateCancel = builder.dateCancel;
    }

    public static class Builder implements CommonModelBuilder<DeliveryResponse> {
        private final Long id;
        private final Long riderId;
        private final String riderName;
        private final Long askId;
        private final String storeName;
        private final String addressStoreDo;
        private final String addressClientDo;
        private final String stateDelivery;
        private final Double distance;
        private final Double priceFood;
        private final Double feeTotal;
        private final String isPay;
        private final String requestRider;
        private final String datePick;
        private final String dateGo;
        private final String dateDone;
        private final String dateCancel;

        public Builder(Delivery delivery) {
            this.id = delivery.getId();
            this.riderId = delivery.getRider().getId();
            this.riderName = delivery.getRider().getName();
            this.askId = delivery.getAsk().getId();
            this.storeName = delivery.getAsk().getStore().getStoreName();
            this.addressStoreDo = delivery.getAsk().getStore().getAddressStoreDo();
            this.addressClientDo = delivery.getAsk().getAddressClientDo();
            this.stateDelivery = delivery.getStateDelivery().getName();
            this.distance = delivery.getAsk().getDistance();
            this.priceFood = delivery.getAsk().getPriceFood();
            this.feeTotal = delivery.getAsk().getPriceRide() * 0.6;
            this.isPay = delivery.getAsk().getIsPay() ? "선" : "후";
            this.requestRider = delivery.getAsk().getRequestRider();
            this.datePick = delivery.getDatePick();
            this.dateGo = delivery.getDateGo();
            this.dateDone = delivery.getDateDone();
            this.dateCancel = delivery.getDateCancel();
        }

        @Override
        public DeliveryResponse build() {
            return new DeliveryResponse(this);
        }
    }
}
