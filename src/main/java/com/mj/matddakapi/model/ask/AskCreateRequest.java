package com.mj.matddakapi.model.ask;

import com.mj.matddakapi.entity.Store;
import com.mj.matddakapi.enums.ask.HowPay;
import com.mj.matddakapi.enums.ask.StateAsk;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
public class AskCreateRequest {
    private String askMenu;
    private Double priceFood;
    private Double priceRide;
    private Boolean isPay;
    private HowPay howPay;
    private String requestStore;
    private String requestRider;
    private String phoneClient;
    private String addressClientDo;
    private String addressClientG;
    private Double xClient;
    private Double yClient;
}
