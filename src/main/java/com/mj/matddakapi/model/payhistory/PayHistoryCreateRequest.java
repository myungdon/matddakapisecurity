package com.mj.matddakapi.model.payhistory;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.payHistory.PayType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayHistoryCreateRequest {
    private Double payAmount;
}
