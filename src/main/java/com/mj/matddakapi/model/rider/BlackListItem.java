package com.mj.matddakapi.model.rider;

import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BlackListItem {
    private String name;

    private String phoneNumber;

    private String isBan;

    private String reasonBan;

    private String dateBan;
}
