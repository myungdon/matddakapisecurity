package com.mj.matddakapi.model.rider;

import com.mj.matddakapi.enums.rider.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter

public class RiderJoinRequest {
//    @NotNull(message = "[Request] 유저 이름은 Null 일 수 없습니다.")
//    @Length(max = 20)
    private String name;

//    @NotNull(message = "[Request] 주민 번호는 Null 일 수 없습니다.")
//    @Length(max = 14)
    private String idNum;

//    @Length(max = 100)
    private String confirmParents;

//    @NotNull(message = "[Request] email은 Null 일 수 없습니다.")
//    @Length(min = 10, max = 40)
    private String email;

//    @NotNull(message = "[Request] password는 Null 일 수 없습니다.")
//    @Length(min = 8, max = 20)
    private String password;

    private String passwordRe;

//    @NotNull(message = "[Request] 핸드폰 번호는 Null 일 수 없습니다.")
//    @Length(max = 20)
    private String phoneNumber;

//    @NotNull(message = "[Request] 기종은 Null 일 수 없습니다.")
//    @Length(max = 20)
    private PhoneType phoneType;

//    @NotNull(message = "[Request] 계좌주는 Null 일 수 없습니다.")
//    @Length(max = 20)
    private String bankOwner;

//    @NotNull(message = "[Request] 은행은 Null 일 수 없습니다.")
//    @Length(max = 20)
    private BankName bankName;

//    @NotNull(message = "[Request] 계좌 주번은 Null 일 수 없습니다.")
//    @Length(max = 14)
    private String bankIdNum;

//    @NotNull(message = "[Request] 계좌 번호는 Null 일 수 없습니다.")
//    @Length(max = 30)
    private String bankNumber;

//    @NotNull(message = "[Request] 희망지역은 Null 일 수 없습니다.")
//    @Length(max = 30)
    private AddressWish addressWish;

//    @NotNull(message = "[Request] 배달 수단은 Null 일 수 없습니다.")
    private DriveType driveType;

//    @NotNull(message = "[Request] 번호판은 Null 일 수 없습니다.")
    private String driveNumber;

//    @NotNull(message = "[Request] 개인정보 약관 동의는 Null 일 수 없습니다.")
    private Boolean agPrivacy;

//    @NotNull(message = "[Request] 위치기반 약관 동의는 Null 일 수 없습니다.")
    private Boolean agLocation;
}
