package com.mj.matddakapi.model.rider.person;

import com.mj.matddakapi.enums.rider.DriveType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class RiderTypeChangeRequest {
    private DriveType driveType;

    private String driveNumber;
}
