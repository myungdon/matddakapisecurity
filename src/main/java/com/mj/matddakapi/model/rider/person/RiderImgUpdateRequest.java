package com.mj.matddakapi.model.rider.person;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RiderImgUpdateRequest {
    private String name;
    private MultipartFile img;
}
