package com.mj.matddakapi.controller;

import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.model.riderpay.RiderPayItem;
import com.mj.matddakapi.model.riderpay.RiderPayResponse;
import com.mj.matddakapi.service.ResponseService;
import com.mj.matddakapi.service.RiderPayService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/rider-pay")
public class RiderPayController {
    private final RiderPayService riderPayService;

    @GetMapping("/all")
    @Operation(summary = "라이더 보유페이 복수")
    public ListResult<RiderPayItem> getRiderPays() {
        return ResponseService.getListResult(riderPayService.getRiderPays(), true);
    }

    @GetMapping("/detail/{riderId}")
    @Operation(summary = "라이더 보유페이 단수")
    public SingleResult<RiderPayResponse> getRiderPay(@PathVariable long riderId) {
        return ResponseService.getSingleResult(riderPayService.getRiderPay(riderId));
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "라이더 보유페이 복수 페이징")
    public ListResult<RiderPayItem> getRiderPayP(@PathVariable int pageNum) {
        return ResponseService.getListResult(riderPayService.getRiderPayP(pageNum), true);
    }
}
