package com.mj.matddakapi.controller;

import com.mj.matddakapi.model.attendance.AttendanceResponse;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.service.AttendanceService;
import com.mj.matddakapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/attendance")
public class AttendanceController {
    private final AttendanceService attendanceService;

    @GetMapping("/detail/{riderId}")
    @Operation(summary = "근태 단수 라이더용")
    public SingleResult<AttendanceResponse> getAttendance(@PathVariable long riderId) {
        return ResponseService.getSingleResult(attendanceService.getAttendance(riderId));
    }

    @PutMapping("/change/pause/{attendanceId}")
    @Operation(summary = "근태 휴식 상태로 변경")
    public CommonResult putAttendancePause(@PathVariable long attendanceId) {
        attendanceService.putAttendancePause(attendanceId);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change/start/{attendanceId}")
    @Operation(summary = "근태 운행 상태로 변경")
    public CommonResult putAttendanceStart(@PathVariable long attendanceId) {
        attendanceService.putAttendanceStart(attendanceId);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change/stop/{attendanceId}")
    @Operation(summary = "근태 퇴근 상태로 변경")
    public CommonResult putAttendanceStop(@PathVariable long attendanceId) {
        attendanceService.putAttendanceStop(attendanceId);

        return ResponseService.getSuccessResult();
    }
}
