package com.mj.matddakapi.controller;

import com.mj.matddakapi.exception.CRiderMissingException;
import com.mj.matddakapi.model.generic.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
    // 보안에 따른 메세지를 주기 위한 컨트롤러
    // 같은반 친구만 의자에 앉을 수 있다.
    // 로그인 안했는데 접속하려고
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CRiderMissingException();
    }
    // 로그인 했지만 가면 안되는 곳 권한이 쌤인 사람만 쌤 의자에 앉을 수 있다.
    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CRiderMissingException();
    }
}
