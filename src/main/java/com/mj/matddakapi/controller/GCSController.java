package com.mj.matddakapi.controller;

import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.service.ResponseService;
import lombok.RequiredArgsConstructor;
import com.mj.matddakapi.service.GCSService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/gcs")
public class GCSController {
    private final GCSService gcsService;

    @PostMapping("/upload")
    public CommonResult objectUpload(@RequestParam("file")MultipartFile file) throws IOException {

        gcsService.uploadObject(file);

        return ResponseService.getSuccessResult();
    }
}
