package com.mj.matddakapi.lib;

public class CommonCheck {
    /**
     * 정규식 왜 써야 할까?
     * 사람들이 입력하는 아이디 "킹왕짱면돈", "404" 이런거 DB에 등록해주면 안되니까
     * 그런데 이거 안된다~~ 라는걸. 아이디는 string이니까 if문으로 처리 할 수 있나?
     * 그래서 정규식 한다
     */
    public static boolean checkEmail(String email) {
        String pattern = "^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$";
        return email.matches(pattern);
    }
}
