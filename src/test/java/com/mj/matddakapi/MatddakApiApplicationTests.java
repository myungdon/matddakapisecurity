package com.mj.matddakapi;

import io.jsonwebtoken.lang.Assert;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItem;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@SpringBootTest
class MatddakApiApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void loadFileCheck() throws IOException {
        File file = new File(new File("").getAbsolutePath() + "/src/main/resources/static/bonche.gif");
        FileItem fileItem = new DiskFileItem("originFile", Files.probeContentType(file.toPath()), false, file.getName(), (int) file.length(), file.getParentFile());

        String test = "1";
    }

    @Test
    void makeRandomName() {
        System.out.println("----------------------");
        System.out.println(UUID.randomUUID());
        System.out.println(LocalDateTime.now());
        System.out.println("----------------------");
    }
}
